import unittest
from main import *



class TestMathsFunctions(unittest.TestCase):
    
    def test_remise0tab(self):
        tab_a_modif = [[1,1,1,1],[1,1,1,1]]
        tab_sortie = [[0,0,0,0],[0,0,0,0]]
        self.assertEqual(remise0tab(tab_a_modif), tab_sortie)


    def test_verifregle(self):
        
        regle1 = IntVar()
        regle2 = IntVar()
        regle1.set(1)
        regle2.set(0)
        
        foret = [[1,1,1,1],[1,1,1,1]]
        i=1
        j=1
        nrow = 4
        ncol = 4

        valeuraretour=1

        self.assertEqual(verifregle(foret,i,j,nrow,ncol,regle1,regle2), valeuraretour)