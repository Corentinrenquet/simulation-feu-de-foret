from tkinter import *
import random
import sys
import argparse





#Moment ou l'on clic sur le plateau pour déclencher un feu
def clic(event):
    j=event.x//cell_size
    i=event.y//cell_size
    if(foret[i][j] == 1):
        Terrain.itemconfigure(carreau[i][j],fill='#FF0000')
        foret[i][j] = 2
        
#Remise à 0 du tableau de passage
def remise0tab(tableaupassage):
    for i in range(len(tableaupassage)):
        for j in range(len(tableaupassage[i])):
            tableaupassage[i][j] = 0
    return tableaupassage

#Fonction lors de l'appuis sur le bt BRULLER
def button_go() :
    elem_to_find=2
    while (any(elem_to_find in sublist for sublist in foret))==True:
        activator.config(state=DISABLED)
        fenetre.after(anim_duration,brulure(foret,tableaupassage,Terrain,carreau,nrow,ncol,regle1,regle2))
        fenetre.update()
    activator.config(state=NORMAL)

#Fonction qui permet de verif et change les états
def brulure(foret,tableaupassage,Terrain,carreau,nrow,ncol,regle1,regle2):
    #Parcour du "tableau" liste impriqués
    for i in range(len(foret)):
        for j in range(len(foret[i])):

            nb2 = random.random()
            
            #Verification si il y a des cendre
            if(foret[i][j] == 3 and tableaupassage[i][j]==0) :
                Terrain.itemconfigure(carreau[i][j],fill = '#FFFFFF')
                foret[i][j] == 0
                tableaupassage[i][j]==1
                

            #Virification si il y a des arbres en feu
            if(foret[i][j] == 2 and tableaupassage[i][j]==0):
                
                #Mise en feu
                if(i<nrow-1):

                    resultatregle = verifregle(foret,i+1,j,nrow,ncol,regle1,regle2)

                    if(foret[i+1][j] == 1 and nb2 > 1-resultatregle):
                        Terrain.itemconfigure(carreau[i+1][j],fill='#FF0000')
                        foret[i+1][j] = 2
                        tableaupassage[i+1][j]=1
                if(i>0):

                    resultatregle = verifregle(foret,i-1,j,nrow,ncol,regle1,regle2)
                    if(foret[i-1][j] == 1 and nb2 > 1-resultatregle):
                        Terrain.itemconfigure(carreau[i-1][j],fill='#FF0000')
                        foret[i-1][j] = 2
                        tableaupassage[i-1][j]=1
                
                if(j<ncol-1):
                    resultatregle = verifregle(foret,i,j+1,nrow,ncol,regle1,regle2)
                    if(foret[i][j+1] == 1 and nb2 > 1-resultatregle):
                        Terrain.itemconfigure(carreau[i][j+1],fill='#FF0000')
                        foret[i][j+1] = 2
                        tableaupassage[i][j+1]=1
                
                if(j>0):
                    resultatregle = verifregle(foret,i,j-1,nrow,ncol,regle1,regle2)
                    if(foret[i][j-1] == 1 and nb2 > 1-resultatregle):
                        Terrain.itemconfigure(carreau[i][j-1],fill='#FF0000')
                        foret[i][j-1] = 2
                        tableaupassage[i][j-1]=1

                #Mise en cendre de l'arbre actuelle
                Terrain.itemconfigure(carreau[i][j],fill = '#808080')
                foret[i][j] = 3
    #Remise du tableau de passage à 0
    tableaupassage = remise0tab(tableaupassage)

    #Fonction permettant de vérif les règles. On lui passe la case qui va être mise en feu si la regle est et sélectionné il ne doit pas y avoir des proba sinon on compte le nb de case en feu autour pour appliquer la formule.
def verifregle(foret,i,j,nrow,ncol,regle1,regle2):
    compteurdefeu = 0
    valeuraretour = 0
    if(regle1.get() == 1) :
        valeuraretour=1
    
    if(regle2.get() == 1):
        if(i<nrow-1):
            if(foret[i+1][j] == 2):
                compteurdefeu = compteurdefeu+1
                
        if(i>0):
            if(foret[i-1][j] == 2):
                compteurdefeu = compteurdefeu+1
                
                
        if(j<ncol-1):
            if(foret[i][j+1] == 2):
                compteurdefeu = compteurdefeu+1
                
                
        if(j>0):
            if(foret[i][j-1] == 2):
                compteurdefeu = compteurdefeu+1
        
        valeuraretour=1-(1/(compteurdefeu+1))
                        
    return valeuraretour 


  
def changementetatactivator() :
    activator.config(state=NORMAL)
    Checkbutton_regle1.config(state=DISABLED)
    Checkbutton_regle2.config(state=DISABLED)

def poucentagearbre(foret) :
    for i in range(len(foret)):
        for elem in range(len(foret[i])):
            nb = random.random()
            if nb > p_boisage:
                foret[i][elem]=1

if  __name__  == "__main__" : 

    parser = argparse.ArgumentParser()
    parser.add_argument("rows", help="nombre de lignes",type=int)
    parser.add_argument("cols", help="nombre de colones",type=int)
    parser.add_argument("anim", help="durée de l'animation en ms",type=int)
    parser.add_argument("cell_size", help="taillle des cellules",type=int)
    parser.add_argument("afforestation", help="pourcentage boisement",type=float)
    args = parser.parse_args()

    #0=Vide 1=Arbre 2=arbre en feu 3=Cendre 

    nrow=args.rows
    ncol=args.cols
    anim_duration=args.anim
    cell_size=args.cell_size
    p_boisage=args.afforestation
    p_boisage=1-((p_boisage)/100)

    foret = [[0 for j in range(ncol)] for i in range(nrow)]
    tableaupassage = [[0] * ncol for _ in range(nrow)]

    poucentagearbre(foret)

    # fenêtre Tkinter
    fenetre=Tk()

    #Titre de la fenêtre
    fenetre.title("Feux forêt")
    #fenetre.geometry("900x900")

    #Taille de la fenêtre
    Terrain=Canvas(fenetre,height=cell_size*nrow,width=cell_size*ncol)
    Terrain.pack(side=TOP)

    #On créer des carrés se 30 par 30 qui seront les zones (on en crée 10*10)
    carreau=[[Terrain.create_rectangle(i*cell_size,j*cell_size,(i+1)*cell_size,(j+1)*cell_size,fill="#FFFFFF") for i in range(ncol)] for j in range(nrow)]

    #Quand on clic gauche sur le canvas
    Terrain.bind('<Button-1>',clic)

    #Bouton permettant de faire BRULLER
    activator = Button(fenetre, text="BRÛLER !", command=button_go,padx=10,pady=10)
    activator.pack(side=TOP)
    activator.config(state=DISABLED)

    #Checkbouton pour les régles si le check est coché alors il y a 1 dans la varible sinon 0 
    regle1 = IntVar()
    Checkbutton_regle1 = Checkbutton(fenetre, text="Regle 1 !",command = changementetatactivator,variable=regle1,onvalue=1,offvalue=0)
    Checkbutton_regle1.pack(side=LEFT)

    regle2 = IntVar()
    Checkbutton_regle2 = Checkbutton(fenetre, text="Regle 2 !",command = changementetatactivator,variable=regle2,onvalue=1,offvalue=0)
    Checkbutton_regle2.pack(side=LEFT)




    #Affichage des arbres au lancement
    for i in range(len(foret)):
        for j in range(len(foret[i])):
            if foret[i][j] == 1 :
                Terrain.itemconfigure(carreau[i][j],fill='#677E52')
    



    fenetre.mainloop()